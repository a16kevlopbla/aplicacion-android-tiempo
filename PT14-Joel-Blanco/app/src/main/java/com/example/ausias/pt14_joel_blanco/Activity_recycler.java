package com.example.ausias.pt14_joel_blanco;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class Activity_recycler extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView t1 = findViewById(R.id.idciutat);
        t1.setText(MainActivity.nomciutat);

        RecyclerView rv1 = findViewById(R.id.recyclerView);
        rv1.setLayoutManager(new LinearLayoutManager(this));

        basedadesSqLite databasetemperatures = new basedadesSqLite(this);


        TemperatureAdapter tadapter = new TemperatureAdapter(databasetemperatures.llegirdb(MainActivity.nomciutat));

        rv1.setAdapter(tadapter);


    }

    public void volver(View view){

        super.onBackPressed();

    }



}
