package com.example.ausias.pt14_joel_blanco;

public class Bloc {
    String hora;
    String temperatura;
    Boolean calor;

    public Bloc (){
        super();
    }

    public Bloc(String hora, String temperatura, Boolean calor){
        this.hora=hora;
        this.temperatura=temperatura;
        this.calor=calor;
    }

    public String getHora() {
        return hora;
    }

    public String getTemperatura() {
        return temperatura;
    }

    public Boolean isCalor() {
        return calor;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setTemperatura(String temperatura) {
        this.temperatura = temperatura;
    }

    public void setCalor(Boolean calor) {
        this.calor = calor;
    }
}