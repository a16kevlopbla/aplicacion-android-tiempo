package com.example.ausias.pt14_joel_blanco;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;
import org.xmlpull.v1.XmlPullParserException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Descarregador extends AsyncTask<String, Void, List<Bloc>> {


    Context context;


    public Descarregador(Context context){
        this.context=context;
    }

    @Override
    protected List<Bloc> doInBackground(String... nomCiutats) {
        //api.openweathermap.org/data/2.5/forecast?q=Barcelona&appid=095199b0a2f8c1e1873a6e7852dd1970

        List<Bloc> b1 = new ArrayList<>();

        try {
            String URLData="http://api.openweathermap.org/data/2.5/forecast?q=";
            String URLAppId="&mode=xml&appid=095199b0a2f8c1e1873a6e7852dd1970";
            URL openWeatherURL = new URL(URLData+nomCiutats[0]+URLAppId);
            HttpURLConnection httpConnection = (HttpURLConnection) openWeatherURL.openConnection();
            try {
                BufferedReader bufr = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
                String xml= "";
                String tmp= "";
                while((tmp=bufr.readLine())!=null){
                    xml = tmp;
                }
                b1 = Parsejador.parseja(xml);
                bufr.close();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } finally {
                httpConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



        return b1;
    }

    @Override
    protected void onPostExecute(List<Bloc> bloc) {
        basedadesSqLite databasetemperatures = new basedadesSqLite(context);


        if (bloc.size()!=0) {
            databasetemperatures.configuraciodb(bloc);

            Intent int1 = new Intent(context, Activity_recycler.class);
            context.startActivity(int1);

        } else {
            Toast.makeText(context, "Aquesta ciutat no existeix", Toast.LENGTH_SHORT).show();
        }

        databasetemperatures.close();

    }
}