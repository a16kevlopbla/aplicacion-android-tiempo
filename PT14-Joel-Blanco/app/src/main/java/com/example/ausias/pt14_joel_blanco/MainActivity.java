package com.example.ausias.pt14_joel_blanco;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {

    /* Hay que utilizar un emulador con API 23 o inferior. Nosotros hemos utilizado API 23 y versión de Android Marshmallow 6.0 */

    static String nomciutat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

    }



    public void open (View view) {

        basedadesSqLite databasetemperatures = new basedadesSqLite(this);

        EditText e1 = findViewById(R.id.editTextCity);
        String nomeditext = e1.getText().toString();
        nomciutat = nomeditext.substring(0, 1).toUpperCase() + nomeditext.substring(1);

            if (databasetemperatures.comprobaciutat(nomciutat)) {
                Descarregador d1 = new Descarregador(this);
                d1.execute(nomciutat);

            } else {
                Intent int1 = new Intent(this,Activity_recycler.class);
                startActivity(int1);
            }

        databasetemperatures.close();
        }

    }


