package com.example.ausias.pt14_joel_blanco;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class Parsejador {

    static List<Bloc> parseja(String xml) throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser parser = factory.newPullParser();

        List<Bloc> listBloc = new ArrayList<Bloc>();
        parser.setInput(new StringReader(xml));
        int eventType = parser.getEventType();
        Bloc bloc;
        bloc = new Bloc();
        while(eventType!=XmlPullParser.END_DOCUMENT) {

            if ((eventType == XmlPullParser.START_TAG) && (parser.getName().equals("time"))) {
                String hora = parser.getAttributeValue(0);
                hora=hora.replace('T','/');
                bloc.setHora(hora);
            }
            if ((eventType == XmlPullParser.START_TAG) && (parser.getName().equals("temperature"))) {
                Float tempF = Float.parseFloat(parser.getAttributeValue(1));
                tempF = tempF - 273.15F;
                Integer tempI = Math.round(tempF);
                bloc.setTemperatura(tempI.toString());
                if(tempI>19){
                    bloc.setCalor(true);
                }else{
                    bloc.setCalor(false);
                }
            }

            if ((eventType == XmlPullParser.END_TAG) && (parser.getName().equals("time"))) {
                listBloc.add(bloc);
                bloc = new Bloc();
            }

            eventType = parser.next();
        }

        if(listBloc.size()>1){
            return listBloc;
        }
        return null;
    }
}
