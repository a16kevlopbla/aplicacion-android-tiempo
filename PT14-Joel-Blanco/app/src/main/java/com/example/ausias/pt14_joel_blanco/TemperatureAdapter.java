package com.example.ausias.pt14_joel_blanco;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class TemperatureAdapter extends RecyclerView.Adapter<TemperatureAdapter.ViewHolder> {

    List<Bloc> bloc;

    public TemperatureAdapter(List<Bloc> bloc){
        this.bloc=bloc;
    }

    @Override
    public TemperatureAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.temperaturafila, parent,false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(TemperatureAdapter.ViewHolder holder, int position) {
        Bloc b1 = bloc.get(position);

        holder.hora.setText("Hora: "+b1.getHora());
        holder.temperatura.setText("Temperatura: "+b1.getTemperatura()+ " ºC");

        if (b1.isCalor())
            holder.icona.setImageResource(R.drawable.sun);
        else {
            holder.icona.setImageResource(R.drawable.cold);
        }
    }

    @Override
    public int getItemCount() {
        return bloc.size();
    }




    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView hora;
        public TextView temperatura;
        public ImageView icona;

        public ViewHolder(View itemView) {
            super(itemView);

            hora = itemView.findViewById(R.id.hora);
            temperatura = itemView.findViewById(R.id.temperatura);
            icona = itemView.findViewById(R.id.icona);
        }
    }

}