package com.example.ausias.pt14_joel_blanco;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.List;


public class basedadesSqLite extends SQLiteOpenHelper {

    Context context;

    public static class Dades {
        public static final String NOM_DB = "temperaturaciutat";
        public static final String NOM_TAULA = "ciutats";
        public static final String COLUMNA_CIUTAT = "nomciutat";
        public static final String COLUMNA_FREDOCALOR = "calorfred";
        public static final String COLUMNA_HORA = "hora";
        public static final String COLUMNA_TEMPERATURA = "temperatura";
    }


    public basedadesSqLite(Context context) {
        super(context, Dades.NOM_DB, null, 1);
        this.context=context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS "+Dades.NOM_TAULA+" ( "+Dades.COLUMNA_CIUTAT+" TEXT, "+
                Dades.COLUMNA_HORA+ " TEXT, " + Dades.COLUMNA_TEMPERATURA + " TEXT , "+
                Dades.COLUMNA_FREDOCALOR + " TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Nothing to do
    }


    public boolean comprobaciutat(String nomciutat) {

        basedadesSqLite databasetemperatures1 = new basedadesSqLite(context);

        SQLiteDatabase dbtemperatura1 = databasetemperatures1.getReadableDatabase();

        System.out.println(Dades.NOM_TAULA);
        Cursor cursor1;

            String sql = "select * from "+Dades.NOM_TAULA+" where "+Dades.COLUMNA_CIUTAT+" = '"+nomciutat+"';";

            cursor1 = dbtemperatura1.rawQuery(sql, null);

            cursor1.moveToNext();
            dbtemperatura1.close();
            databasetemperatures1.close();
        if (cursor1.getCount()>0){
                cursor1.close();
                return false;
            } else
                cursor1.close();
                return true;
    }


    public void configuraciodb(List<Bloc> bloc1) {

        basedadesSqLite databasetemperatures2 = new basedadesSqLite(context);

        SQLiteDatabase dbtemperatura2 = databasetemperatures2.getWritableDatabase();


        for(int i=0;i<bloc1.size();i++){

                ContentValues valors = new ContentValues();
                valors.put(Dades.COLUMNA_FREDOCALOR, bloc1.get(i).isCalor());
                valors.put(Dades.COLUMNA_TEMPERATURA, bloc1.get(i).getTemperatura());
                valors.put(Dades.COLUMNA_HORA, bloc1.get(i).getHora());
                valors.put(Dades.COLUMNA_CIUTAT, MainActivity.nomciutat);

                dbtemperatura2.insert("'"+Dades.NOM_TAULA+"'", null, valors);
        }

        dbtemperatura2.close();
        databasetemperatures2.close();
    }

    public List<Bloc> llegirdb(String nomciutat){

        basedadesSqLite databasetemperatures3 = new basedadesSqLite(context);

        SQLiteDatabase dbtemperatura3 = databasetemperatures3.getReadableDatabase();

        List<Bloc> aBloc1 = new ArrayList<Bloc>();


        Cursor c1 = dbtemperatura3.rawQuery("select * from '"+Dades.NOM_TAULA+"' where "+Dades.COLUMNA_CIUTAT+" = '"+nomciutat+"';", null);

        while (c1.moveToNext()){

            String hora = c1.getString(c1.getColumnIndexOrThrow(Dades.COLUMNA_HORA));
            String temperatura = c1.getString(c1.getColumnIndexOrThrow(Dades.COLUMNA_TEMPERATURA));
            int calorfred = c1.getInt(c1.getColumnIndexOrThrow(Dades.COLUMNA_FREDOCALOR));

            Boolean boolean1 = new Boolean(true);
            if (calorfred == 1)
                boolean1 = true;
            else if (calorfred == 0)
                boolean1 = false;

            Bloc b1 = new Bloc(hora, temperatura, boolean1);
            aBloc1.add(b1);

        }

        dbtemperatura3.close();
        databasetemperatures3.close();
        return aBloc1;
    }

}